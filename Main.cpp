#include <iostream>
#include <string>


int main()
{
	std::cout << "Enter any word: ";
	std::string Word;   // Word = 1234567890
	std::getline(std::cin, Word);

	long len = Word.length();
	std::string  first = Word.substr(0, 1);
	std::string  last = Word.substr(len-1, 1);

	char f = Word.at(0);
	char l = Word.at(len-1);


	std::cout << "Your word: " << Word << std::endl;
	std::cout << "First char: " << first << std::endl;
	std::cout << "Last char: " << last << std::endl;
	std::cout << "Quantity char: " << len << std::endl;

}

	
